package geekbrains.zero.a3l2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;

public class FABHideOnScroll extends FloatingActionButton.Behavior {

    private static final String TAG = "FABHideOnScroll";

    public FABHideOnScroll(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onNestedScroll(@NonNull CoordinatorLayout coordinatorLayout, @NonNull FloatingActionButton child, @NonNull View target, int dxConsumed, int dyConsumed, int dxUnconsumed, int dyUnconsumed, int type) {
        super.onNestedScroll(coordinatorLayout, child, target, dxConsumed, dyConsumed, dxUnconsumed, dyUnconsumed, type);
//        if (child.getVisibility() == View.VISIBLE && dyConsumed > 0) {
//            child.setVisibility(View.INVISIBLE);
//        } else if (child.getVisibility() == View.INVISIBLE && dyConsumed < 0) {
//            child.setVisibility(View.VISIBLE);
//        }
//        if (child.getVisibility() == View.VISIBLE && dyConsumed > 0) {
//            child.hide();
//        } else if (child.getVisibility() == View.GONE && dyConsumed < 0) {
//            child.show();
//        }
        if (dyConsumed > 0) {
            CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) child.getLayoutParams();
            int fab_bottomMargin = layoutParams.bottomMargin;
            child.animate().translationY(child.getHeight() + fab_bottomMargin).setInterpolator(new LinearInterpolator()).start();
        } else if (dyConsumed < 0) {
            child.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
        }
    }

    @Override
    public boolean onStartNestedScroll(
            @NonNull CoordinatorLayout coordinatorLayout,
            @NonNull FloatingActionButton child,
            @NonNull View directTargetChild,
            @NonNull View target,
            int axes,
            int type) {
        printNowType(axes);
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }

    private void printNowType(int type) {
        String s;
        switch (type) {
            case ViewCompat.SCROLL_AXIS_NONE:
                s = "NONE";
                break;
            case ViewCompat.SCROLL_AXIS_HORIZONTAL:
                s = "HORIZONTAL";
                break;
            case ViewCompat.SCROLL_AXIS_VERTICAL:
                s = "VERTICAL";
                break;
            default:
                s = "";
                break;
        }
        Log.d(TAG, "printNowType: " + s);
    }
}
